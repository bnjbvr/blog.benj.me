Title: Slide decks
Slug: slides
Summary: My slide decks

### 2024

- [The state of the Matrix Rust SDK in 2023 / FOSDEM](https://fosdem.org/2024/events/attachments/fosdem-2024-3283-the-state-of-the-matrix-rust-sdk-in-2023/slides/22029/FOSDEM_State_of_Rust_SDK_in_2023_eFY77Fu.pdf)

### 2023

- [Les outils pour compiler plus rapidement en Rust / Rust Lyon](https://framagit.org/rustlyonmeetup/rustlyonmeetup.frama.io/-/blob/main/talk-2/output.pdf?ref_type=heads)
- [WebAssembly, les cas d'utilisations et évolutions récentes / MixIT](https://bnjbvr.github.io/slides/2023/wasm-mixit/)

### 2022

- [WebAssembly, histoire et fonctionnement](https://bnjbvr.github.io/slides/2022/wasm-january/#1)

### 2018

- [Des apps Web rapides avec WebAssembly (Rust + JVM) / JUG Lausanne September 2018](http://bnjbvr.github.io/slides/2018/wasm-jug)
- [Des apps Web rapides avec WebAssembly / MixIT 2018](http://bnjbvr.github.io/slides/2018/wasm-mixit)
- [Spectre & Meltdown / INSA Lyon - April 2018](https://PandiPanda69.github.io/INSHACK2k18-Meltdown-Spectre)
- [Kresus / Jeudi du libre - March 2018](http://bnjbvr.github.io/slides/2018/jdl-kresus)

### 2017

- [Philosophie et logiciel libre - JDLL 2017](http://bnjbvr.github.io/slides/2017/jdll-philo/prez.pdf)
- [WebAssembly pour les développeurs / MixIT April 2017](http://bnjbvr.github.io/slides/2017/mixit-wasm)

### 2016

- [Kresus - Meetup cozy / JDLL 2016](http://bnjbvr.github.io/slides/2016/kresus)
- [Rien de trop / JDLL 2016](http://bnjbvr.github.io/slides/2016/rien-de-trop)
- [WebAssembly / LyonJS September 2016](http://bnjbvr.github.io/slides/2016/lyonjs-wasm)
- [WebAssembly / ParisWeb November 2016](http://bnjbvr.github.io/slides/2016/parisweb-wasm)

### 2015

- [Future of JS - FOSDEM](http://bnjbvr.github.io/slides/2015/javascript-future-fosdem-2015)
- [Human Talks February](http://bnjbvr.github.io/slides/2015/humantalks-2015-feb)
- [ES6 - JDLL](http://bnjbvr.github.io/slides/2015/jdll-2015-es6)
- [Future of JS / WASM - MLOC](http://bnjbvr.github.io/slides/2015/mlocjs-2015)
- [ES6 Performance - LyonJS November](http://bnjbvr.github.io/slides/2015/lyonjs-11-2015/es6-perf.html)
- [Promise error handling - LyonJS November](http://bnjbvr.github.io/slides/2015/lyonjs-11-2015/promises-error-handling.html)

### 2014

- [Performance in JS / Mozilla Paris](http://bnjbvr.github.io/slides/2014/mozillaparis-javascript-performance-2014)
