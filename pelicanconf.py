#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Benjamin Bouvier'
SITENAME = u"Benjamin Bouvier's world"
SITEURL = ''

PATH = 'content'
STATIC_PATHS = ['img']
ARTICLE_PATHS = ['blog']
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_LANG_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
ARTICLE_LANG_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'

THEME = 'themes/modern'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'

# Theme specific
SITETITLE=u"Benjamin Bouvier"
SITELOGO=u"/img/logo.jpeg"

COPYRIGHT_YEAR=u"2015 - 2024"
MAIN_MENU=True

LINKS = (
    ('Resume', 'https://bouvier.cc/pub/resume-2023-02-09.pdf'),
)

SOCIAL = (
     ('gitlab', 'https://framagit.org/bnjbvr'),
     ('github', 'https://github.com/bnjbvr'),
     ('youtube', 'https://www.youtube.com/channel/UCqBNTOCHBfSzieU1wvnXsmQ/videos'),
     ('soundcloud', 'https://soundcloud.com/bnjbvr'),
     ('linkedin', 'https://www.linkedin.com/in/bnjbvr'),
)

# Fathom
FATHOM_URL = '1984.b.delire.party'
FATHOM_SITE_ID = 'CSQLE'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FEED_DOMAIN = SITEURL
FEED_ALL_RSS = u'rss'
TAG_FEED_RSS = u'tag/{slug}.rss'
RSS_FEED_SUMMARY_ONLY = False

DEFAULT_PAGINATION = False

JINJA_ENVIRONMENT = {'extensions': []}

PLUGINS = ['minchin.pelican.plugins.post_stats']

ISSO_BASE_URL = 'https://saywhat.benj.me'

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {'anchorlink': True},
    },
    'output_format': 'html5',
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
